#!/usr/bin/env bash
#debug=1
FileLocation=/usr/share/applications/

App_Names=""
declare -A A_App_Names

## read ls output
while read a; do
  [ $debug ] && echo $a
  ## Grep First 'Name=' and capture name after
  N=`grep '^Name=' "$a" | head -1 | cut -f2 -d=`;
  ## check if Name Are present
  if [ ! -z "$N" ]; then
    ## Check if Name already exist in array
    if [[ -v "A_App_Names['$N']" ]] ; then
      [ $debug ] && echo "$N - already in Array"
    else
      [ $debug ] && echo "$N - will be added to Array"
      ## Add Name to Key in array and add File location to varible
      A_App_Names+=([$N]=$a)
    fi;
  else
    [ $debug ] && echo  $a" - Has no Name in file = "$N
  fi;
done < <(ls -1 $FileLocation*.desktop)

## Sort list
App_Names=$( for k in "${!A_App_Names[@]}"
do
  printf "%s\n" "$k"
done | sort )

## add \\n between word list for dmenu to devide selection
App_Names=${App_Names//$'\n'/\\n}

[ $debug ] && echo $App_Names

## open Dmenu wait for selection
Selection=$( echo -e $App_Names | dmenu )

[ $debug ] && echo "You selected "$Selection
[ $debug ] && echo ${A_App_Names[$Selection]}

## check if selction is Null
if [ -n "$Selection" ]; then
  [ $debug ] && echo Selection String Not empty = $Selection
  ## Open .desktop
  kioclient5 exec ${A_App_Names[$Selection]} 
else
  [ $debug ] && echo Selection String empty = NULL
fi


