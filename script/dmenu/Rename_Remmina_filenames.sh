#!/bin/bash
cd ~/.local/share/remmina/    # or ~/.config/remmina/ if appropriate
ls -1 *.remmina | while read a; do
       N=`grep '^name=' "$a" | cut -f2 -d=`;
       P=`grep '^protocol=' "$a" | cut -f2 -d=`;
       [ "$a" == "$N-$P.remmina" ] || mv "$a" "$N-$P".remmina;
done
