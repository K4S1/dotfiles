#!/usr/bin/env bash
#debug=1
remminaFileLocation=~/.local/share/remmina/
FolderLocation=~/.config/remmina-dmenu

#if [ ! -d $FolderLocation ]; then
#  [ $debug ] && echo $FolderLocation Not Exists, will create
#  mkdir -p $FolderLocation;
#else
#  [ $debug ] && echo $FolderLocation Exist
#fi

## Get Remmina Files
servernames=$(fd -a -e "remmina" . $remminaFileLocation | sed 's|\(.*\)\/||;s/.remmina//')
## Replace Enters \n with Char \n because Dmenu don't catch
servernames=${servernames//$'\n'/\\n}



Selection=$(echo -e $servernames | dmenu ) 

echo $Selection

if [ -n "$Selection" ]; then
  [ $debug ] && echo $FolderLocation \$Selection String Not empty = $Selection
  find $remminaFileLocation -name "*$Selection*" | xargs -0 -I{} xdg-open "{}" 
else
  [ $debug ] && echo $FolderLocation \$Selection String empty = NULL
fi


#File=$( find $remminaFileLocation -name "*$Selection*" )
#[ $debug ] && echo $File
#ls -la "$File"
#xargs -0 -I{} xdg-open "{}" "$File"
#$File
#
#find $remminaFileLocation -name "*$Selection*" | xargs -0 -I{} xdg-open "{}" 

###
#declare -A Str_A_Hosts
#
#
### Try Get Hostname from inside File
#ls -1 $remminaFileLocation*.remmina | while read a; do
#  N=`grep '^name=' "$a" | cut -f2 -d=`;
#  P=`grep '^protocol=' "$a" | cut -f2 -d=`;
#  #echo $N
#  Str_A_Hosts+=([$(echo $N)]=foo)
#done
#
#
##echo ${Str_A_Hosts[@]}
#for key in "${!Str_A_Hosts[@]}"; do echo "$key"; done
